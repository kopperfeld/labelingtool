
# Globales, statisches Objekt
# Speichert Daten und Funktionen, die von überall aus zugänglich sein sollen (um Funktionsparameter zu sparen)
# Vorsicht: Besser regeln, welche Objekte in My schreiben und aus My lesen sollen (klare Datenflussrichtungen)


class My:

	mainWindow = None   # VideoPlot Labeling (QMainWindow)
	labelEditor = None   # LabelType Editor (QWidget)
	signalChooser = None  # Plot SignalChooser (QWidget)
	
	class Plot:
	
		canvas = None   # Plot Canvas zum Zeichnen des Plot-Graphen, Koordinatensystem und Beschriftung
		dataFrame = []  # Aktueller Plot Datenrahmen (?)
		xData = []      # Plot-Achsendaten Horizontal
		yData = []      # Plot-Achsendaten Vertikal
		filePath = ''   # Verzeichnispfad der Plotdatei
		signals = []
		signalsToPlot = []
		useTimeline = False
	
	class Data:
	
		folder = 'data'  # Unterverzeichnis aller "Bearbeitungsdaten" (Videos, Plots, Labels)
		fileExt = 'csv'  # Erwartete Dateierweiterung für Signaldaten-File
		fileSfx1 = 'FReABCIn'  # Erwartetes Dateisuffix für Signaldaten-File Typ-1
		fileSfx2 = 'EML_wo_GPS'  # Erwartetes Dateisuffix für Signaldaten-File Typ-2
		fileSfx3 = 'merged'  # Erwartetes Dateisuffix für Signaldaten-File Typ-2
		fileID = 'nnnn_yyyymmddhhmmss'  # Erwartetes Prefix-Format aller Bearbeitungsdateien (Video, Plots, Labels)
		# fileID = 'yyyymmddhhmmss'  # Erwartetes Prefix-Format aller Bearbeitungsdateien (Video, Plots, Labels)
		# fileIDSize = -10  # Erwartete konstante Länge des Datei-Prefix
		fileIDEnd = 19  # Erwartete Länge des Datei-Prefix (konstant)
	
		@staticmethod  # Dateiname als Überschrift zur visuellen Überprüfung (nicht funktional)
		def titleText():
			return My.Data.fileID + '_' + My.Data.fileSfx3 + '.' + My.Data.fileExt
	
	class Label:
	
		fileSfx = 'labels'  # erwarteter suffix im Dateinamen ("_labels")
		fileExt = 'csv'  # erwartete Dateiendung und Dateityp
		typeFile = 'labelTypes.json'  # labelTyp-Datei, enthält eine Liste aller Labelarten
		typeFileX = 'labelTypesX.json'  # testfile für den import neuer Labelarten
		testFileX = 'labelTester.csv'  # reines testfile (wird wohl nicht mehr benutzt/gebraucht)
	
		@staticmethod  # Dateiname als Überschrift zur visuellen Überprüfung (nicht funktional)
		def titleText():
			return My.Data.fileID + '_' + My.Label.fileSfx + '.' + My.Label.fileExt
	
	class Video:
	
		fileExt = 'avi'  # erwartete(r) Dateiendung und Dateityp
		fileSfx = 'video'  # erwarteter suffix im Dateinamen ("_video")
		fileName = 'nnnn_yyyymmddhhmmss.avi'  # Vorheriges Format für Dateiname (wird durch echten überschrieben)
		# fileName = 'yyyymmddhhmmss.avi'  # erwartetes NEUES Format für Dateiname (wird bei Programmstart überschrieben)
		frameRateAvg = 12  # Mittelwert aus stichprobenartig gefundenen Bildraten der Videos (9 und 15 fps)
		currentPosition = 700
	
		@staticmethod  # Dateiname als Überschrift (just for visual check, non-functional)
		def titleText():
			return My.Data.fileID + '_' + My.Video.fileSfx + '.' + My.Video.fileExt
	
	class Config:
	
		folder = 'config'  # Unterverzeichnis für Datei "LabelTypes.json" (und weiterer Konfigurationsdateien)
