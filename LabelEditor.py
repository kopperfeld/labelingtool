
import json
import MainWindow

from com.My import My
from labelEditor.WinTools import WinTools
from gui.UiLabelEditor import Ui_LabelEditor
from mainWindow.WinMonitor import Flags

from BaseWindow import BaseWindow


class LabelEditor(Ui_LabelEditor, BaseWindow):

	def __init__(self, mw: MainWindow, labels):
	
		self.mw = mw
		self.labels = labels
		self.labelsX = None
		self.tools = WinTools(self)
		self.setupUi()
		self.updateLabels(labels)
	
	def setupUi(self):
	
		super(LabelEditor, self).setupUi(My.labelEditor)
	
		self.btnSave.clicked.connect(self.buttonSaveClick)
		self.btnCancel.clicked.connect(self.buttonCancelClick)
	
		self.btnLabInsert.clicked.connect(self.buttonInsertClick)
		self.btnLabDelete.clicked.connect(self.buttonDeleteClick)
		self.btnLabEdit.clicked.connect(self.buttonEditClick)
		self.btnLabReplace.clicked.connect(self.buttonReplaceClick)
		self.btnLabImport.clicked.connect(self.buttonImportClick)
	
		self.cmbLabFarbe.clear()
		self.cmbLabFarbe.insertItem(0, "...")
		self.cmbLabFarbe.addItem("Rot")  # TODO: Farben evtl lieber als Liste einfügen (addItems)
		self.cmbLabFarbe.addItem("Orange")
		self.cmbLabFarbe.addItem("Gelb")
		self.cmbLabFarbe.addItem("Lemon")
		self.cmbLabFarbe.addItem("Gruen")  # TODO: Entscheiden ob Umlaute besser wären
		self.cmbLabFarbe.addItem("Cyan")
		self.cmbLabFarbe.addItem("Blau")
		self.cmbLabFarbe.addItem("Magenta")
		self.cmbLabFarbe.addItem("Braun")
		self.cmbLabFarbe.addItem("Schwarz")
		self.cmbLabFarbe.addItem("Dunkelgelb")
		self.cmbLabFarbe.addItem("Dunkelgrau")
		self.cmbLabFarbe.addItem("Dunkelgruen")
		self.cmbLabFarbe.addItem("Dunkelblau")
	
	def buttonImportClick(self):
	
		path = self.openFileDialog('Öffne Labeldatei')
		if path != '':
			self.labelsX = self.loadLabels(path)
			added = self.addNewLabels()
			self.updateLabels(self.labels)
			self.showDialogOk("Information", str(added) + " neue Labels wurden hinzugefuegt")
		else:
			self.showDialogOk("Fehler", "Pfad nicht gefunden")
		
	def buttonSaveClick(self):
		
		self._backToMainWindow()
	
	def buttonCancelClick(self):
		
		self._backToMainWindow()

	def updateLabels(self, labs):
		
		self.listLabNames.clear()
		for l in labs:
			self.listLabNames.addItem(l['LabelName'])
			self.colorLastItem(l['LabelFarbe'])
	
	def colorLastItem(self, t):
	
		i = self.listLabNames.count() - 1
		self.listLabNames.item(i).setForeground(self.tools.getColor(t))
	
	def loadLabels(self, fileName):
		
		file = open(fileName)
		labs = json.load(file)
		return labs
	
	def addNewLabels(self):
		
		added = 0
		for l in self.labelsX:
			if not self.labelExists(l['LabelName'], self.labels):
				added += 1
				self.labels.append(l)
		return added
	
	def labelExists(self, name, labs):
		
		for l in labs:
			if name == l['LabelName']: return True
		return False
	
	def buttonReplaceClick(self):
	
		sl = self.listLabNames.selectedItems()
		for l in sl:
			l.setText(self.txtLabName.text())
			l.setForeground(self.tools.chosenColor())
			break
	
	def buttonInsertClick(self):
	
		labText = self.txtLabName.text()
		if labText != "":
			i = self.listLabNames.count()
			self.listLabNames.insertItem(i, self.txtLabName.text())
			self.listLabNames.item(i).setForeground(self.tools.chosenColor())
			self.txtLabName.setText('')
	
	def buttonDeleteClick(self):
	
		sl = self.listLabNames.selectedItems()
		print(sl)
		for l in sl:
			self.listLabNames.takeItem(self.listLabNames.row(l))
	
	def buttonEditClick(self):
	
		sl = self.listLabNames.selectedItems()
		if len(sl) >= 1:
			self.txtLabName.setText(sl[0].text())
	
	def _backToMainWindow(self):
		
		self.mw.monitor.setFlag(Flags.BackToMain)
		My.labelEditor.close()

