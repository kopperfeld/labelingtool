
import MainWindow
from com.My import My

from PyQt5.QtCore import QTime
from PyQt5.QtWidgets import QStyle
from PyQt5.QtMultimedia import QMediaPlayer


class VideoPlayer:

	def __init__(self, mw: MainWindow):

		self.mw = mw
		self.ratePos = 3                            	  # Startindex sollte '1.0' der rateList entsprechen
		self.rateList = [0.1, 0.25, 0.5, 1.0, 1.5, 2.0]  # Geschwindigkeitsfaktoren Videowiedergabe/Videospulen

	# to be called when user presses any playPause-control in the window
	def playPause(self):

		if self._isPlaying():
			self.mw.mediaPlayer.pause()
		else:
			self.mw.mediaPlayer.play()

	# call this when you want to set video-position to a specific value [ms]
	def setPosition(self, position):

		self.mw.mediaPlayer.setPosition(position)

	def storePosition(self):

		My.Video.currentPosition = self.mw.mediaPlayer.position()

	# gets called whenever videoPlayer state changes (play, pause, stop, end)
	def stateChanged(self):

		if self.mw.mediaPlayer.state() == QMediaPlayer.PlayingState:
			self.mw.playButton.setIcon(My.mainWindow.style().standardIcon(QStyle.SP_MediaPause))
		else:
			self.mw.playButton.setIcon(My.mainWindow.style().standardIcon(QStyle.SP_MediaPlay))

		# print("VideoPlayer new state = " + str(self.mw.mediaPlayer.state()))
		# print("VideoPlayer cur position = " + str(self.mw.mediaPlayer.position()))

	# gets called whenever video plays, or user moves slider or winds video (single-frame buttons)
	def positionChanged(self, position):

		self.mw.positionSlider.setValue(position)
		self._updateTimeLabel(position)

	# gets called right after new video-file is loaded
	def durationChanged(self, duration):

		self.mw.positionSlider.setRange(0, duration)
		self.mw.mediaPlayer.setPosition(0)
		self.mw.mediaPlayer.pause()
		self.applyPlayRate()

	# gets called when video-file not found or video-format not supported
	def handleError(self):

		self.mw.playButton.setEnabled(False)
		self.mw.errorLabel.setText("Error: " + self.mw.mediaPlayer.errorString())

	# returns play position in milliseconds
	def playPosition(self):

		return self.mw.mediaPlayer.position()

	# returns play duration in milliseconds
	def playDuration(self):

		return self.mw.mediaPlayer.duration()

	# returns unified/normalized play position
	def playTime(self):

		p = self.mw.mediaPlayer.position()
		d = self.mw.mediaPlayer.duration()

		return 0.0 if d < 0.001 else p / d  # division durch 0 vermeiden

	# 'style' should be 1 or 2 (skipped 'format' as it's a reserved keyword)
	def playTimeText(self, style):

		pos = self.mw.mediaPlayer.position()

		z3 = (pos % 1000)         # mils (zzz)
		s2 = (pos // 1000) % 60   # secs (ss)
		m1 = (pos // 60000) % 10  # mins (m)

		s1 = str(s2).zfill(1) + ':' + str(z3).zfill(3)
		s2 = str(m1).zfill(1) + ':' + str(s2).zfill(2) + ':' + str(z3).zfill(3)

		return s1 if style < 2 else s2

	# update button and label text according to play state; periodically called by 'WinMonitor'
	def updateControls(self):

		if self._isPlaying():
			self.mw.btnPlayPause.setText("||")
			# self.mw.labPlayPause.setText(" pause")
		else:
			self.mw.btnPlayPause.setText(">")
			# self.mw.labPlayPause.setText(" play")

	def updatePosition(self):

		p = self.mw.mediaPlayer.position()
		self.mw.mediaPlayer.setPosition(p)

	# wind/spool video by some frames (signed)
	def moveByFrames(self, frames):

		self._correctState()
		self.moveByTime(self._timeSpanOf(frames))

	# wind/spool video by timeSpan [ms] (signed)
	def moveByTime(self, dt):

		t = self.mw.mediaPlayer.position()
		d = self.mw.mediaPlayer.duration()

		p2 = self.mw.tools.clamp(t + dt, 0, d)
		self.setPosition(p2)

	# change playback rate by step (signed)
	def movePlayRate(self, step):

		count = len(self.rateList)
		self.ratePos = self.mw.tools.clamp(self.ratePos + step, 0, count - 1)

	# read and set current playback rate
	def applyPlayRate(self):

		self.mw.mediaPlayer.setPlaybackRate(self.readPlayRate())

	# read current playback rate from array
	def readPlayRate(self):

		return self.rateList[self.ratePos]

	# update play-time label text, whenever video position changes
	def _updateTimeLabel(self, ms):

		zzz = ms % 1000  # mils
		ss = (ms // 1000) % 60  # secs
		m = (ms // 60000) % 10  # mins
		self.mw.timLabTime.setTime(QTime(0, m, ss, zzz))

	# convert frames to timeSpan [ms]
	def _timeSpanOf(self, frames):

		dt = (frames * 1000) / My.Video.frameRateAvg  # print("_timeStep() {0:.3f}".format(dt) + " msecs")
		return dt

	# return whether videoPlayer is in playing-state
	def _isPlaying(self):

		return self.mw.mediaPlayer.state() == QMediaPlayer.PlayingState

	# a) ensure videoPlayer is in paused-state when user presses a "step frame" button
	# b) ensure videoPlayer stays at the end when reached end-of-video
	def _correctState(self):

		st = self.mw.mediaPlayer.state()

		if st == QMediaPlayer.PlayingState:
			self.mw.mediaPlayer.pause()

		if st == QMediaPlayer.StoppedState or st == QMediaPlayer.EndOfMedia:
			self.mw.mediaPlayer.setPosition(self.mw.mediaPlayer.duration())
			self.mw.mediaPlayer.pause()
