import MainWindow
import numpy as np
from enum import Enum
from com.My import My
from PyQt5.QtCore import QTimer

# Grund für WinMonitor: Video-Abspielen mit flexibel einstellbarer Geschwindigkeit (z.B. 0.5, 1.0, 2.0) direkt mit
# QMediaPlayer-Funktionen ist bisher fehlgeschlagen (führte zum Absturz).
# Annahme Problemursache: Funktionsfähigkeit hängt vom Videoformat ab, und 'avi' wird nicht (zuverlässig) unterstützt.
#
# Hauptzweck ist es bestimmte Zustandsänderungen in der App (speziell im MainWindow) zu erkennen,
# welche nicht durch die (in der PyQt5) integrierten Events unterstützt werden.
# Pattern: Diese Klasse implementiert das Observer Pattern in vereinfachter und angepasster Form.
#
# Dazu wurde ein QTimer eingerichtet, welcher die Methode "update" periodisch aufruft (hier 40ms/25fps, anpassbar).
# Konkret werden damit die VideoPlayer-Buttons 'FrameBack' und 'FrameFore' überwacht und deren 'Hold'-state erkannt
# - sprich: ob der Button für eine gewisse Zeit gedrückt und gehalten wurde.
# Der Hold-State wiederum wird gebraucht, um Slow/Fast Motion beim Video-Spulen zu emulieren. Dieses erledigt
# die Methode 'updateVideoPlayer' (s.u.).
# Weiteres Ziel ist das echte Abspielen der Videos mit Slow/Mast Motion, was nur derzeit zu viel technischen Aufwand
# (Codemenge) erzeugt und deshalb in die Zukunft verschoben wurde.


class WinMonitor:

	def __init__(self, mw: MainWindow):
	
		self.mw = mw
		self.frame = 0      # Frame Counter. Zur Vereinfachung wird 'frame' als Zeiteinheit verwendet
		self.period = 50    # 40 ms => 25 Updates pro Sekunde (guter Erfahrungswert, änderbar)
		self.period2 = 50   # 80 ms => 12,5 Updates/Sek. für PlotCanvas
		self.holdSpan = 4   # set hold-state after so many frames (12 ~ 0.5 secs, changeable)#
		self.plotDraw = False
		self.timer1 = QTimer()
		self.timer2 = QTimer()
		self.btnFrameBack = Button(self, "back")  # Observer-Objekt für frame-backward button
		self.btnFrameFore = Button(self, "fore")  # Observer-Objekt für frame-forward button
		self.flags = np.empty(len(Flags), dtype='b')  # jedes Flag enstpricht einem Event => Action
		# self.flags = np.full(len(Flags), False, dtype='b')
	
	def setup(self):
		
		self.timer1.setInterval(self.period)  # set update-interval
		self.timer2.setInterval(self.period2)  # set update-interval
		self.timer1.timeout.connect(self.update1)
		self.timer2.timeout.connect(self.update2)
		self.timer1.start()
		self.timer2.start()
		self.clearFlags()
	
	def update1(self):
		
		self.frame += 1  # print("timerUpdate() frame = " + str(self.frame))
		self._updateVideoPlayer()
		self._updateWindowStates()
	
	def update2(self):
		
		if My.Plot.useTimeline:
			self._updatePlotCanvas(True)
		elif self.plotDraw:
			self._updatePlotCanvas(False)

	def _updateWindowStates(self):

		self.mw.events.updateLabelStates()
		
	def _updatePlotCanvas(self, again):  # 'again' means if plot should be drawn once more

		self.plotDraw = again
		self.mw.player.storePosition()
		self.mw.events.updatePlotCanvas()

	def _updateVideoPlayer(self):
	
		rate = self.mw.player.readPlayRate()  # play/wind rate (scale)
		speed = self.period * rate            # play/wind speed [ms]
		speedX = "{:0.2f}".format(rate)       # play/wind speed (formatted text)
	
		self.mw.labPlayRate.setText(speedX)
	
		# update button states
		self.btnFrameBack.update(self.frame, self.mw.btnFrameBack.isDown())
		self.btnFrameFore.update(self.frame, self.mw.btnFrameFore.isDown())
	
		# update button and label text
		self.mw.player.updateControls()
	
		# apply slow-motion function
		if self.btnFrameFore.hold:
			self.mw.player.moveByTime(speed)  # wind video forward by timeStep [ms]
		if self.btnFrameBack.hold:
			self.mw.player.moveByTime(-speed)  # wind video backward by timeSpep [ms]
		
		# evaluate flag-actions
		self.execFlags()
		self.clearFlags()
	
	def setFlag(self, flag):
		
		self.flags[flag.value] = True
	
	def clearFlags(self):
		
		for i in range(len(self.flags)):
			self.flags[i] = False
	
	def execFlags(self):
		
		if self.flags[Flags.BackToMain.value]:
			self.mw.player.updatePosition()  # make video show again


# Die Enum-Klasse 'Flags' dient dazu, WinMonitor-Flags von ausserhalb "per name" setzen zu können.
# Jedes Flag gehört zu einem Event und daraus folgender Action (Codesequenz)
# Die Actions werden in 'execFlags()' ausgeführt, falls das jeweilge Flag gesetzt wurde.
class Flags(Enum):
	
	Nothing = 0
	BackToMain = 1
	BackToSome = 2


# Die Klasse Button überwacht und speichert alle 'interessanten' Zustände je eines bestimmten
# 'QPushButtons' im MainWindow.
class Button:

	name = ""
	down: bool = False  # in down-state?
	hold: bool = False  # in hold-state?
	span: int = 0       # down-time in frames
	frame: int = 0      # was downed at frame
	
	def __init__(self, wm: WinMonitor, name):
	
		self.wm = wm
		self.name = name
		self.down = False
		self.hold = False
		self.span = 0
		self.frame = 0
	
	def update(self, frame, down):  # gets called periodically
	
		pressed = not self.down and down  # button-pressed event?
		released = self.down and not down  # button-released event?
	
		if pressed or released:  # update/reset down-frame on both events
			self.frame = frame
	
		self.down = down  # store current down-state
		self.span = frame - self.frame  # store current down-span
		self.hold = self.down and self.span > self.wm.holdSpan  # have hold-state?
		# self._log()
	
	def _log(self):  # show states for debug/test reasons
	
		if self.down or self.hold:
			print("button " + self.name + "; frames " + str(self.span) +
					"; down " + str(self.down) + "; hold " + str(self.hold))
