
import os
import sys
import logging

import pandas
import MainWindow

from com.My import My
from pathlib import Path
from PyQt5.QtCore import QUrl, QSettings
from PyQt5.QtMultimedia import QMediaContent


class WinEvents:

	def __init__(self, mw: MainWindow):
	
		self.mw = mw
	
	def onFileLoad(self):
	
		fpath = self.mw.openFileDialog("Open Video", My.Data.folder)
		if fpath != '':
			self.loadFiles(fpath)
	
	# Das passiert nach dem im "Load File" Dialog ein AVI-Video (Annahme) gewählt wurde
	def loadFiles(self, fpath):
	
		self._playerProcess(fpath)
		self._fileProcess(fpath)
	
		if self._havePlotFile():
			self._loadLabels()  # 1. zuerst das labelfile laden (Reihenfolge wichtig)
			self._loadPlots()   # 2. dannach das plotfile laden (Reihenfolge wichtig)

	# hänge PlotLabel ans Listenende... mehr siehe _appendPlotLabel()
	def onButtonAppend(self):
	
		lab = self.mw.labels.createLabel()
		self._appendPlotLabel(lab)
	
	# lösche PlotLabel aus Liste... mehr siehe _deletePlotLabel()
	def onButtonDelete(self):
	
		row = self.mw.listLabels.currentRow()
		if row >= 0:
			self._deletePlotLabel(row)
	
	def onButtonEditor(self):
	
		print('Clicked button "Editor"')
	
	def onButtonSignalChooser(self):
	
		print('Clicked button "SignalChooser"')
	
	def onButtonPlayPause(self):
	
		self.mw.player.playPause()
	
	def onButtonFramePrev(self):
	
		rate = self.mw.player.readPlayRate()
		self.mw.player.moveByFrames(-rate)
	
	def onButtonFrameNext(self):
		
		rate = self.mw.player.readPlayRate()
		self.mw.player.moveByFrames(rate)
	
	def onButtonPlaySlower(self):
	
		self.mw.player.movePlayRate(-1)
		self.mw.player.applyPlayRate()
	
	def onButtonPlayFaster(self):
	
		self.mw.player.movePlayRate(1)
		self.mw.player.applyPlayRate()
		
	def onButtonTimeline(self):
		
		use = not My.Plot.useTimeline
		text = "Timeline on" if use else "Timeline off"
		self.mw.btnTimeline.setText(text)
		My.Plot.useTimeline = use

	def updateLabelStates(self):

		row = self.mw.listLabels.currentRow()
		item = self.mw.listLabels.currentItem()

		if item and item.isSelected():
			self.mw.labels.focusLabel(row, True)
		else:
			self.mw.labels.focusLabel(-1, False)
	
	def updatePlotCanvas(self):
		
		# print("_updatePlotCanvas()...")
		mp = My.Plot
		try:
			mp.canvas.plot(mp.xData, mp.yData, self.mw.labels.list)
		except Exception as e:
			logging.error("plot error:" + str(e))
	
	# ==== internal methods ======================== #

	# exisitiert das entsprechende PlotFile (CSV-Signaldaten)?
	def _havePlotFile(self):

		path = Path(My.Plot.filePath)
		have = path.is_file()

		if not have: self.mw.showDialogOk2("Error Message", "Missing Plot-File")
		return have
	
	def _playerProcess(self, filePath):
	
		self.mw.mediaPlayer.setMedia(QMediaContent(QUrl.fromLocalFile(filePath)))
		self.mw.playButton.setEnabled(True)
	
	def _fileProcess(self, filePath):
	
		md = My.Data
		md.fileID = self._extractFileID(filePath)
	
		self.mw.labVideoName.setText('Video  <' + My.Video.titleText() + '>')
		self.mw.labPlotName.setText('Signal Plot  <' + My.Data.titleText() + '>')
		self.mw.labLabelName.setText('Labels  <' + My.Label.titleText() + '>')
	
		# TODO: Change absolute path back to relative path. Reason: Loading fails if path contains Umlaut (ö,ü,ä)
		My.Label.testFileX = os.path.dirname(filePath) + "/" + md.fileID + "_" + "labels.csv"
		My.Plot.filePath = os.path.dirname(filePath) + "/" + md.fileID + "_" + md.fileSfx3 + "." + md.fileExt
		# print("_fileProcess() mp.filePath = " + My.Plot.filePath)
	
	# extract file-id (file prefix) from path, used to create output filenames
	def _extractFileID(self, filePath):
		
		path = os.path.normpath(filePath)
		names = path.split(os.sep)
		index = len(names) - 1	 # (unsafely) assuming index >= 0
		name = names[index]  	 # (unsafely) assuming name is last substring
		return name[0:My.Data.fileIDEnd]
		
	def _playerProcess(self, filePath):
	
		self.mw.mediaPlayer.setMedia(QMediaContent(QUrl.fromLocalFile(filePath)))
		self.mw.playButton.setEnabled(True)
	
	def _loadPlots(self):
	
		mp = My.Plot  # print("_loadPlots() path = " + mp.FilePath)
	
		try:
			mp.dataFrame = pandas.read_csv(mp.filePath, sep=';', skiprows=[1], index_col=None)
		except Exception as e:
			logging.error("read csv exception:" + str(e))
	
		dfm = mp.dataFrame
	
		try:
			headers = self._loadSettings("Signals/SignalsToPlot")
		except Exception as e:
			logging.error("load setting error" + str(e))
		try:
			mp.yData = dfm[headers]
		except Exception as e:
			logging.error("headers loading error" + str(e))
	
		mp.signalsToPlot = headers
		mp.signals = [item for item in list(dfm) if item not in headers]
	
		timeBase = dfm[dfm.columns[0]].iloc[0]  # expecting value '0' from xxx_merged.csv
		timeStamp = 'ts_relative'               # changed header in xxx_merged.csv (previously: 'timestamp')
	
		dfm[timeStamp] = dfm[timeStamp].apply(lambda x: (x - timeBase))
	
		mp.xData = dfm[timeStamp].apply(lambda x: (x / 1000000))  # time axes begins from 0 and converted from milliseconds to seconds
	
		try:
			mp.canvas.plot(mp.xData, mp.yData, self.mw.labels.list)
		except Exception as e:
			logging.error("plot error:" + str(e))
	
	def _loadLabels(self):

		self.mw.labels.listLoadFromFile()
		self.mw.labels.listUpdate()
		self.mw.labels.setFocusChanged()
	
	# function loads stored settings when loading data file
	def _loadSettings(self, key):
		try:
			PATH = os.path.realpath(os.path.dirname(sys.argv[0]))
		except Exception as e:
			logging.error("path error:" + str(e))
	
		configFileRelativePath = "config\\setting.ini"
		configFilePath = os.path.join(PATH, configFileRelativePath)
	
		self.settings = QSettings(configFilePath, QSettings.IniFormat)
		try:
			currSet = self.settings.value(key)
		except Exception as e:
			logging.error("error load setting" + str(e))
		return self.settings.value(key)
	
		pass

	# hänge PlotLabel ans ListenEnde, Scrolle ans Ende, Speichere neue Liste ab
	def _appendPlotLabel(self, lab):
	
		# self.o.labels.showAppDialog(lab)
		self.mw.labels.listAppend(lab)
		self.mw.labels.listUpdate()
		self.mw.labels.listSaveToFile()
		self.mw.listLabels.scrollToBottom()
		self.mw.labels.setFocusChanged()
		self.updatePlotCanvas()  # TODO: Replace this by 'monitor.plotDraw=True'

	# lösche PlotLabel aus Liste, Speichere geänderte Liste ab, reSelektiere aktuelle Zeile
	def _deletePlotLabel(self, row):
	
		self.mw.labels.listRemove(row)         # clear list in view (mainWindow.listLabels)
		self.mw.labels.listUpdate()            # rebuild list in view (mainWindow.listLabels)
		self.mw.labels.listSaveToFile()        # immediately persist all plotLabels to file
		self.mw.listLabels.setCurrentRow(row)  # try reSelect current row for repeated deletion
		self.mw.labels.setFocusChanged()
		self.updatePlotCanvas()  # TODO: Replace this by 'monitor.plotDraw=True'
