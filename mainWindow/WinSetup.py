
import json
import MainWindow

from com.My import My
from mainWindow.PlotCanvas import PlotCanvas

from PyQt5 import QtWidgets
from PyQt5.QtMultimediaWidgets import QVideoWidget
from PyQt5.QtWidgets import QHBoxLayout, QSizePolicy, QStyle, QVBoxLayout


class WinSetup:

	def __init__(self, mw: MainWindow):
	
		self.mw = mw
	
	def setupUi(self):
	
		self._setComponents(self.mw)
		self._setMediaPlayer(self.mw)
	
	def _setComponents(self, mw: MainWindow):  # setup User Interface components
	
		mw.btnLabAppend.clicked.connect(mw.buttonAppendClick)
		mw.btnLabDelete.clicked.connect(mw.buttonDeleteClick)
		mw.btnLabEditor.clicked.connect(mw.buttonEditorClick)
		mw.btnSignalChooser.clicked.connect(mw.buttonSignalChooserClick)
		mw.actionLoad.triggered.connect(mw.menuFileLoad)
	
		mw.btnFrameBack.pressed.connect(mw.events.onButtonFramePrev)
		mw.btnFrameFore.pressed.connect(mw.events.onButtonFrameNext)
		mw.btnPlayPause.pressed.connect(mw.events.onButtonPlayPause)
		mw.btnPlaySlower.pressed.connect(mw.events.onButtonPlaySlower)
		mw.btnPlayFaster.pressed.connect(mw.events.onButtonPlayFaster)
		mw.btnTimeline.pressed.connect(mw.events.onButtonTimeline)
		
		mw.canvas = PlotCanvas(mw.plotWidget, width=8, height=4)  # plot setup
		
		self._loadComboBox()
		# self.clearLabels()
	
		My.Plot.canvas = mw.canvas
		My.labelEditor = QtWidgets.QWidget()
	
	def _setMediaPlayer(self, mw: MainWindow):  # setup VideoPlayer components
	
		vidWidget = QVideoWidget()
	
		mw.playButton.clicked.connect(mw.events.onButtonPlayPause)
		mw.playButton.setEnabled(False)
		mw.playButton.setIcon(My.mainWindow.style().standardIcon(QStyle.SP_MediaPlay))
		mw.positionSlider.sliderMoved.connect(mw.player.setPosition)
		mw.positionSlider.setRange(0, 0)
		mw.errorLabel.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Maximum)
	
		# Create layouts to place inside widget
		contLayout = QHBoxLayout()
		contLayout.setContentsMargins(0, 0, 0, 0)
		contLayout.addWidget(mw.playButton)
		contLayout.addWidget(mw.positionSlider)
	
		layout = QVBoxLayout()
		layout.addWidget(vidWidget)
		layout.addLayout(contLayout)
		layout.addWidget(mw.errorLabel)
	
		# Set widget to contain window contents
		mw.videoWidget.setLayout(layout)
	
		mw.mediaPlayer.setVideoOutput(vidWidget)
		mw.mediaPlayer.setNotifyInterval(40)
		mw.mediaPlayer.stateChanged.connect(mw.player.stateChanged)
		mw.mediaPlayer.positionChanged.connect(mw.player.positionChanged)
		mw.mediaPlayer.durationChanged.connect(mw.player.durationChanged)
		mw.mediaPlayer.error.connect(mw.player.handleError)
	
	def _loadComboBox(self):
	
		self.mw.cmbLabName.clear()
		file = open(My.Config.folder + '/' + My.Label.typeFile, 'r')
		items = json.load(file)
	
		for item in items:
			self.mw.cmbLabName.addItem(item['LabelName'])
	
		self.mw.items = items
