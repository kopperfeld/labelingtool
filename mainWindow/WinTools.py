
import os
import MainWindow

from mainWindow.PlotLabels import PlotLabel


class WinTools:

    def __init__(self, mw: MainWindow):

        self.mw = mw

    # löscht die PlotLabel-Liste (intern und im Fenster-listWidget)
    def clearLabels(self):

        self.mw.listLabels.clear()

    # hängt ein neues Label and die PlotLabel-liste (intern und im listWidget)
    def appendLabel(self, lab: PlotLabel):

        self.mw.listLabels.addItem(lab.toRowText())

    # gibt den in der ComboBox ausgewählten/angezeigten LabelName-Text zurück
    def currentLabelText(self):

        return self.mw.cmbLabName.currentText()

    # erstellt die Datei 'path' falls sie noch nicht existiert
    def makeFileExist(self, path):

        if not os.path.exists(path):
            file = open(path, 'w')
            file.close()

    def getVideoPath(self, pfx, sfx, ext):

        return pfx + '_' + sfx + '.' + ext

    # begrenzt den Wert 'val' auf den Bereich 'vmin' bis 'vmax'
    def clamp(self, val, vmin, vmax):

        return max(vmin, min(val, vmax))

