import csv
import logging
import traceback
import MainWindow

from com.My import My


class PlotLabel:

    id: int = 0         # list index [0..n] (to show in list-widget)
    time: float = 0     # time value [0..1] normalized (maybe used later)
    playPos: int = 0    # play position [ms] (used mainly for plotting)
    playDur: int = 0    # play duration [ms] (just for completeness)
    nameText: str = ""  # display name (well-formatted to show in plot)
    timeText: str = ""  # display time (well-formatted for list widget)

    def __init__(self, index=-1, time=0, pPos=0, pDur=0, lName="---", lTime="mm:ss:zzz"):

        self.id = index
        self.time = time
        self.playPos = pPos
        self.playDur = pDur
        self.nameText = lName
        self.timeText = lTime
        self.focused = False  # is label selected in labelList?

    # PlotLabel in ein CSV-konformes Array unwandeln (zur Speicherung als Zeile in einer CSV-Datei)
    def toList(self):

        return [self.id, self.time, self.playPos, self.playDur, self.nameText, self.timeText]

    # PlotLabel Werte aus CSV-Array einlesen (Zeile wurde zuvor aus einer CSV-Datei geladen)
    def fromList(self, ss):

        n = len(ss)
        if n > 0: self.id = int(ss[0])
        if n > 1: self.time = float(ss[1])
        if n > 2: self.playPos = int(ss[2])
        if n > 3: self.playDur = int(ss[3])
        if n > 4: self.nameText = str(ss[4])
        if n > 5: self.timeText = str(ss[5])

    # Dient zu Überprüfung der Werte (derzeit nicht benutzt)
    def logOut(self):

        print("logOut label id: " + str(self.id))
        print("logOut label time: " + str(self.time))
        print("logOut label playPos: " + str(self.playPos))
        print("logOut label playDur: " + str(self.playDur))
        print("logOut label nameText: " + str(self.nameText))
        print("logOut label timeText: " + str(self.timeText))

    def toRowText(self):

        return '#' + str(self.id + 1) + ' | ' + self.timeText + ' | ' + self.nameText


class PlotLabels:

    def __init__(self, mw: MainWindow):

        self.mw = mw
        self.list = []  # list of PlotLabels
        self.timeMs = 0
        self.labName = ''
        self.lastRow = -1  # previously focused label (to detect changes)

    def setup(self):

        self.listUpdate()

    # PlotLabel an das Ende der Labelliste hängen
    def listAppend(self, lab: PlotLabel):

        # print("listAppend"); lab.logOut()
        lab.id = len(self.list)
        self.list.append(lab)

    # PlotLabel an der markierten Stelle aus der Labelliste löschen
    def listRemove(self, row):

        if row < len(self.list):
            del self.list[row]

    # Labelliste in der Anzeige (im ListWidget) erneuern
    def listUpdate(self):  # print("listUpdate()")

        # clear list (in widget)
        self.mw.tools.clearLabels()

        # reindex list (internal)
        i = 0
        for lab in self.list:
            lab.id = i
            i += 1

        # populate list (in widget)
        for lab in self.list:  # print("listUpdate() lab = " + str(lab))
            self.mw.tools.appendLabel(lab)

    # Labelliste in der aktuellen CSV-Datei speichern (erfolgt nach jeder Änderung)
    def listSaveToFile(self):  # print("listUpdate()")

        path = self._currentFilePath()

        try:
            file = open(path, 'w', newline='')
            writer = csv.writer(file, delimiter=',', quotechar='"')

            for lab in self.list:
                writer.writerow(lab.toList())

        except Exception as e:
            print(e)

        file.close()

    # Labelliste aus der aktuellen CSV-Datei laden (erfolgt nur zu Programmstart)
    def listLoadFromFile(self):

        path = self._currentFilePath()
        self.mw.tools.makeFileExist(path)

        try:
            file = open(path, 'r')
            reader = csv.reader(file, delimiter=',', quotechar='"')
            self.list.clear()

            for row in reader:
                lab = PlotLabel()
                lab.fromList(row)
                self.list.append(lab)

        except Exception as e:
            print(e)

        file.close()

    def setFocusChanged(self):

        self.lastRow = -1

    def focusLabel(self, row, sel):  # print("focusLabel()")

        if row != self.lastRow:
            self.mw.monitor.plotDraw = True
        self.lastRow = row

        for lab in self.list:
            lab.focused = (lab.id == row) and sel

    def createLabel(self):

        idx = -1
        time = self.mw.player.playTime()
        ppos = self.mw.player.playPosition()
        pdur = self.mw.player.playDuration()
        sname = self.mw.tools.currentLabelText()
        stime = self.mw.player.playTimeText(2)

        return PlotLabel(idx, time, ppos, pdur, sname, stime)

    # Funktion wird derzeit nicht benutzt bzw. aufgerufen
    def storeLabelData(self):  # store labelData into labelDataFile

        log = logging.getLogger('logger')
        fileNm = My.Label.testFileX; print("current labelFile: " + fileNm)

        try:
            file = open(fileNm, 'a+'); print("start reading...")

            self.labName = self.mw.cmbLabName.currentText(); print("current labelName:" + self.labName)
            self.timeMs = 50037  # timeMs = self.mediaPlayer.position()

            row = [self.timeMs, self.labName]

            writer = csv.writer(file)
            writer.writerow(row)
            file.close()

        except Exception as e:
            log.error('file open / csv write error: ' + str(e))
            log.error(traceback.format_exc())

    # Funktion wird derzeit nicht benutzt bzw. aufgerufen
    def showAppDialog(self, lab: PlotLabel):

        # font = QFont(self.o.cmbLabName.currentText())
        self.mw.showDialogOk2("Label Hinzugefügt", "Name: " + lab.nameText + "\nZeit: " + lab.timeText)

    # Gibt Verzeichnispfad + Dateiname der aktuellen CSV-Labeldatei zurück
    def _currentFilePath(self):

        return My.Data.folder + "/" + My.Data.fileID + "_" + My.Label.fileSfx + "." + My.Label.fileExt
