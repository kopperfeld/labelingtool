
from com.My import My
from PyQt5.QtWidgets import QSizePolicy

from matplotlib.figure import Figure
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas


class PlotCanvas(FigureCanvas):

	def __init__(self, parent=None, width=15, height=4, dpi=100):
	
		fig = Figure(figsize=(width, height), dpi=dpi)
		self.axes = fig.add_subplot(111)
	
		FigureCanvas.__init__(self, fig)
		self.setParent(parent)
	
		FigureCanvas.setSizePolicy(self, QSizePolicy.Expanding, QSizePolicy.Expanding)
		FigureCanvas.updateGeometry(self)
		self.init_plot()
	
	def init_plot(self):
	
		self.axes.set_title('Data Plot')
		self.axes.grid()
	
		self.draw()
		self.show()
	
	def plot(self, xData, yData, labelList):
	
		self.axes.clear()
		self.axes.set_xlabel("Zeit [s]")
		self.axes.set_ylabel("Signale")
		self.axes.plot(xData, yData)
		self.axes.legend(tuple(My.Plot.signalsToPlot), fontsize='xx-small')
		self.axes.set_xlim(left=0.0)
		self.axes.grid()
		
		self._addTimeLine()  # print(labelList)

		for label in labelList:
			self._addLabelTag(label)

		self.draw()

	def _addTimeLine(self):

		if My.Plot.useTimeline:
			pos = My.Video.currentPosition / 1000
			self.axes.axvline(pos, linewidth=1.0, color='black')

	def _addLabelTag(self, lab):

		col = 'red' if lab.focused else 'blue'

		self.axes.axvline(lab.playPos / 1000, linewidth=0.3, color=col)
		self.axes.text(lab.playPos / 1000, -12, lab.nameText, size='small', rotation='vertical',
						bbox={'facecolor': 'white', 'edgecolor': col, 'alpha': 0.5, 'pad': 3})
