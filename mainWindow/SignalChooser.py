
import sys
import os
from PyQt5.QtWidgets import QApplication, QWidget, QListWidget
from PyQt5.QtCore import QSettings
from gui.UiSignalChooser import Ui_SignalChooser
from com.My import My
import logging
import MainWindow
from BaseWindow import BaseWindow


class SignalChooser(Ui_SignalChooser):

	def __init__(self, labels):
	
		self.labels = labels
		self.setupUi()
	
	def setupUi(self):
	
		super(SignalChooser, self).setupUi(My.signalChooser)
	
		initSignList = My.Plot.signals
		initPlotSignList = My.Plot.signalsToPlot
	
		self.signalsListWidget.addItems(My.Plot.signals)
	
		self.signalsToPlotListWidget.addItems(My.Plot.signalsToPlot)
	
		self.addButton.clicked.connect(
			lambda *args: self.moveCurrentItem(self.signalsListWidget, self.signalsToPlotListWidget))
		self.removeButton.clicked.connect(
			lambda *args: self.moveCurrentItem(self.signalsToPlotListWidget, self.signalsListWidget))
		self.saveButton.clicked.connect(lambda *args: self.saveSignalSelection())
		self.cancelButton.clicked.connect(lambda *args: self.buttonCancelClick())
		self.pushButton.clicked.connect(lambda *args: self.buttonOKClick())
		pass
	
	def moveCurrentItem(self, srcListWidget, trgListWidget):
	
		try:
			trgListWidget.addItem(srcListWidget.takeItem(srcListWidget.currentIndex().row()))
		except Exception as e:
			logging.error("move item error:"+str(e))
	
	
	def saveSignalSelection(self):
		
		self.updatePlotCanvas()

		self.saveSetting("Signals/SignalsToPlot", My.Plot.signalsToPlot)
		self.saveSetting("Signals/Signals", My.Plot.signals)
		
	def updatePlotCanvas(self):
		
		mp = My.Plot
		
		mp.signalsToPlot = [self.signalsToPlotListWidget.item(i).text()
							for i in range(self.signalsToPlotListWidget.count())]
		mp.signals = [self.signalsListWidget.item(i).text()
						for i in range(self.signalsListWidget.count())]
		mp.yData = mp.dataFrame[mp.signalsToPlot]
		
		mp.canvas.plot(mp.xData, mp.yData, self.labels)
	
	def saveSetting(self, key, value):
	
		try:
			PATH = os.path.realpath(os.path.dirname(sys.argv[0]))
		except Exception as e:
			logging.error("path error:" + str(e))
	
		configFileRelativePath = "config\\setting.ini"
		configFilePath = os.path.join(PATH, configFileRelativePath)
	
		try:
			self.settings = QSettings(configFilePath, QSettings.IniFormat)
			self.settings.setValue(key, value)
			self.settings.sync()
		except Exception as e:
			logging.error("setting error:" + str(e))
	
		pass
	
	def buttonOKClick(self):
	
		self.saveSignalSelection()
		My.signalChooser.close()
	
	def buttonCancelClick(self):
	
		try:
			My.signalChooser.close()
		except Exception as e:
			logging.error("cancellation error:" + str(e))
