
import LabelEditor

from PyQt5.QtGui import QColor
from PyQt5.QtCore import Qt


class WinTools:

    def __init__(self, window: LabelEditor):

        self.mw = window

    def onSomeKey(self):
        print("onSomeKey")

    def chosenColor(self):

        t = self.mw.cmbLabFarbe.itemText(self.mw.cmbLabFarbe.currentIndex()); print(t)
        return self.getColor(t)

    def getColor(self, t):

        if t == 'Rot': return Qt.red
        if t == 'Orange': return QColor(255, 153, 0)  # QColor.fromRgbF(1, 0.6, 0)
        if t == 'Gelb': return Qt.yellow
        if t == 'Lemon': return QColor(127, 255, 0)
        if t == 'Gruen': return Qt.green
        if t == 'Cyan': return Qt.cyan
        if t == 'Blau': return Qt.blue
        if t == 'Magenta': return Qt.magenta
        if t == 'Braun': return Qt.darkRed
        if t == 'Schwarz': return Qt.black
        if t == 'Dunkelgelb': return Qt.darkYellow
        if t == 'Dunkelgrau': return Qt.darkGray
        if t == 'Dunkelgruen': return Qt.darkGreen
        if t == 'Dunkelblau': return Qt.darkBlue
        return Qt.gray


