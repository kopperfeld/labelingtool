
from com.My import My
from PyQt5.QtCore import QDir
from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import QFileDialog, QMessageBox


class BaseWindow:

    def openFileDialog(self, title, folder=''):  # 'folder' is subfolder

        path = QDir.currentPath()
        if len(folder) > 0:
            path += "/" + folder

        fpath, _ = QFileDialog.getOpenFileName(My.mainWindow, title, path)
        return fpath

    def showDialogOk(self, title, text):

        msg = QMessageBox()
        msg.setIcon(QMessageBox.Information)
        msg.setWindowTitle(title)
        msg.setText(text)
        msg.setStandardButtons(QMessageBox.Ok)
        msg.exec_()

    def showDialogOk2(self, title, text, fsize=12):

        msg = QMessageBox()
        msg.setFont(QFont('Consolas', fsize))
        msg.setIcon(QMessageBox.Information)
        msg.setWindowTitle(title)
        msg.setText(text)
        msg.setStandardButtons(QMessageBox.Ok)
        msg.exec_()

