# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'UiSignalChooser.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_SignalChooser(object):
    def setupUi(self, SignalChooser):
        SignalChooser.setObjectName("SignalChooser")
        SignalChooser.resize(663, 356)
        SignalChooser.setWindowOpacity(14.0)
        SignalChooser.setAccessibleName("")
        self.gridLayoutWidget = QtWidgets.QWidget(SignalChooser)
        self.gridLayoutWidget.setGeometry(QtCore.QRect(30, 20, 581, 261))
        self.gridLayoutWidget.setObjectName("gridLayoutWidget")
        self.gridLayout = QtWidgets.QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setContentsMargins(10, 10, 10, 10)
        self.gridLayout.setSpacing(10)
        self.gridLayout.setObjectName("gridLayout")
        self.SignalsScrollArea = QtWidgets.QScrollArea(self.gridLayoutWidget)
        self.SignalsScrollArea.setWidgetResizable(True)
        self.SignalsScrollArea.setObjectName("SignalsScrollArea")
        self.scrollAreaWidgetContents = QtWidgets.QWidget()
        self.scrollAreaWidgetContents.setGeometry(QtCore.QRect(0, 0, 559, 239))
        self.scrollAreaWidgetContents.setObjectName("scrollAreaWidgetContents")
        self.signalsToPlotScrollArea = QtWidgets.QScrollArea(self.scrollAreaWidgetContents)
        self.signalsToPlotScrollArea.setGeometry(QtCore.QRect(300, 40, 181, 171))
        self.signalsToPlotScrollArea.setWidgetResizable(True)
        self.signalsToPlotScrollArea.setObjectName("signalsToPlotScrollArea")
        self.scrollAreaWidgetContents_2 = QtWidgets.QWidget()
        self.scrollAreaWidgetContents_2.setGeometry(QtCore.QRect(0, 0, 179, 169))
        self.scrollAreaWidgetContents_2.setObjectName("scrollAreaWidgetContents_2")
        self.signalsToPlotListWidget = QtWidgets.QListWidget(self.scrollAreaWidgetContents_2)
        self.signalsToPlotListWidget.setGeometry(QtCore.QRect(10, 10, 161, 151))
        self.signalsToPlotListWidget.setObjectName("signalsToPlotListWidget")
        self.signalsToPlotScrollArea.setWidget(self.scrollAreaWidgetContents_2)
        self.addButton = QtWidgets.QToolButton(self.scrollAreaWidgetContents)
        self.addButton.setGeometry(QtCore.QRect(250, 90, 25, 19))
        self.addButton.setArrowType(QtCore.Qt.RightArrow)
        self.addButton.setObjectName("addButton")
        self.removeButton = QtWidgets.QToolButton(self.scrollAreaWidgetContents)
        self.removeButton.setGeometry(QtCore.QRect(250, 120, 25, 19))
        self.removeButton.setArrowType(QtCore.Qt.LeftArrow)
        self.removeButton.setObjectName("removeButton")
        self.signalsToPlotScrollArea_2 = QtWidgets.QScrollArea(self.scrollAreaWidgetContents)
        self.signalsToPlotScrollArea_2.setGeometry(QtCore.QRect(40, 40, 181, 171))
        self.signalsToPlotScrollArea_2.setWidgetResizable(True)
        self.signalsToPlotScrollArea_2.setObjectName("signalsToPlotScrollArea_2")
        self.scrollAreaWidgetContents_3 = QtWidgets.QWidget()
        self.scrollAreaWidgetContents_3.setGeometry(QtCore.QRect(0, 0, 179, 169))
        self.scrollAreaWidgetContents_3.setObjectName("scrollAreaWidgetContents_3")
        self.signalsListWidget = QtWidgets.QListWidget(self.scrollAreaWidgetContents_3)
        self.signalsListWidget.setGeometry(QtCore.QRect(10, 10, 161, 151))
        self.signalsListWidget.setObjectName("signalsListWidget")
        self.signalsToPlotScrollArea_2.setWidget(self.scrollAreaWidgetContents_3)
        self.label = QtWidgets.QLabel(self.scrollAreaWidgetContents)
        self.label.setGeometry(QtCore.QRect(40, 10, 47, 16))
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(self.scrollAreaWidgetContents)
        self.label_2.setGeometry(QtCore.QRect(300, 10, 101, 21))
        self.label_2.setObjectName("label_2")
        self.SignalsScrollArea.setWidget(self.scrollAreaWidgetContents)
        self.gridLayout.addWidget(self.SignalsScrollArea, 0, 1, 1, 1)
        self.horizontalLayoutWidget = QtWidgets.QWidget(SignalChooser)
        self.horizontalLayoutWidget.setGeometry(QtCore.QRect(370, 300, 289, 31))
        self.horizontalLayoutWidget.setObjectName("horizontalLayoutWidget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.pushButton = QtWidgets.QPushButton(self.horizontalLayoutWidget)
        self.pushButton.setObjectName("pushButton")
        self.horizontalLayout.addWidget(self.pushButton)
        self.saveButton = QtWidgets.QPushButton(self.horizontalLayoutWidget)
        self.saveButton.setObjectName("saveButton")
        self.horizontalLayout.addWidget(self.saveButton)
        self.cancelButton = QtWidgets.QPushButton(self.horizontalLayoutWidget)
        self.cancelButton.setObjectName("cancelButton")
        self.horizontalLayout.addWidget(self.cancelButton)

        self.retranslateUi(SignalChooser)
        QtCore.QMetaObject.connectSlotsByName(SignalChooser)

    def retranslateUi(self, SignalChooser):
        _translate = QtCore.QCoreApplication.translate
        SignalChooser.setWindowTitle(_translate("SignalChooser", "Signale auswählen"))
        self.addButton.setText(_translate("SignalChooser", "..."))
        self.removeButton.setText(_translate("SignalChooser", "..."))
        self.label.setText(_translate("SignalChooser", "Signale"))
        self.label_2.setText(_translate("SignalChooser", "Signale zu plotten"))
        self.pushButton.setText(_translate("SignalChooser", "OK"))
        self.saveButton.setText(_translate("SignalChooser", "Anwenden"))
        self.cancelButton.setText(_translate("SignalChooser", "Abbrechen"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    SignalChooser = QtWidgets.QWidget()
    ui = Ui_SignalChooser()
    ui.setupUi(SignalChooser)
    SignalChooser.show()
    sys.exit(app.exec_())

