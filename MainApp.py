
import sys

from PyQt5 import QtWidgets

from com.My import My
from MainWindow import MainWindow

if __name__ == "__main__":

    app = QtWidgets.QApplication(sys.argv)

    My.mainWindow = QtWidgets.QMainWindow()
    My.labelEditor = QtWidgets.QWidget()
    My.signalChooser = QtWidgets.QWidget()

    MW = MainWindow()  # must store MW-reference to avoid garbage collecting
    My.mainWindow.show()

    sys.exit(app.exec_())
