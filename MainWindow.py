
import os
import json

from com.My import My
from gui.UiMainWindow import Ui_MainWindow

from mainWindow.WinSetup import WinSetup
from mainWindow.WinTools import WinTools
from mainWindow.WinEvents import WinEvents
from mainWindow.PlotLabels import PlotLabels
from mainWindow.WinMonitor import WinMonitor
from mainWindow.VideoPlayer import VideoPlayer

from BaseWindow import BaseWindow
from LabelEditor import LabelEditor

from PyQt5.QtCore import Qt
from PyQt5.QtMultimedia import QMediaPlayer
from PyQt5.QtWidgets import QLabel, QSlider, QPushButton, QAction, QLineEdit, QWidget

from mainWindow.SignalChooser import SignalChooser


# TODO: Rename Ui_VideoEditor, UiVideoEditor to 'MainWindow' scheme
class MainWindow(Ui_MainWindow, BaseWindow):

	def __init__(self):
	
		self.LE = None  # stored reference keeps LabelEditor from being garbage-collected
		self.SC = None  # stored reference keeps SignalChooser from being garbage-collected
	
		self.mediaPlayer = QMediaPlayer(None, QMediaPlayer.VideoSurface)
		self.playButton = QPushButton()
		self.positionSlider = QSlider(Qt.Horizontal)
		self.errorLabel = QLabel()
		self.setup = WinSetup(self)
		self.tools = WinTools(self)
		self.events = WinEvents(self)
		self.labels = PlotLabels(self)
		self.monitor = WinMonitor(self)
		self.player = VideoPlayer(self)
		self.signal = SignalChooser(self)
	
		self.canvas = None  # plot canvas
		self.indexFile = -1
		self.selectedFiles = []
	
		super(MainWindow, self).setupUi(My.mainWindow)
	
		self.labdict = {}  # speichert die PlotLabel-Shortcuts
		self.loadLabelNames()
		print(self.labdict)
	
		self.labels.setup()
		self.monitor.setup()
		self.setup.setupUi()
		self.setupFileMenu()
	
	def menuFileLoad(self):
		self.events.onFileLoad()
	
	# Todo update plot when appending label
	def buttonAppendClick(self):
		self.events.onButtonAppend()
	
	# Todo update plot when deleting label
	def buttonDeleteClick(self):
		self.events.onButtonDelete()
	
	def buttonEditorClick(self):
		self.events.onButtonEditor()
		self._spawnLabelEditor()
	
	def buttonSignalChooserClick(self):
		self.events.onButtonSignalChooser()
		self._spawnSignalChooser()
	
	def _spawnLabelEditor(self):
		self.LE = LabelEditor(self, self.items)  # need to store LE reference, pass TypeLabels
		My.labelEditor.show()
	
	def _spawnSignalChooser(self):
		self.SC = SignalChooser(self.labels.list)  # need to store SC reference, pass PlotLabels
		My.signalChooser.show()
	
	# === file menu setup ===>
	
	def setupFileMenu(self):
	
		tb = My.mainWindow.addToolBar("File")
		start = QAction("Start", My.mainWindow)
		start.setShortcut("Ctrl+Shift+S")
		start.triggered.connect(self.getFiles)
		tb.addAction(start)
		self.previousFile = QAction("Zurück", My.mainWindow)
		self.previousFile.setShortcut("Ctrl+P")
		self.previousFile.triggered.connect(self.getPreviousFile)
		self.previousFile.setDisabled(True)
		tb.addAction(self.previousFile)
		self.filenumber = QLineEdit()
		self.filenumber.setReadOnly(True)
		self.filenumber.setText("Start")
		self.filenumber.setFixedWidth(60)
		tb.addWidget(self.filenumber)
		self.nextFile = QAction("Nächste", My.mainWindow)
		self.nextFile.setShortcut("Ctrl+N")
		self.nextFile.triggered.connect(self.getNextFile)
		self.nextFile.setDisabled(True)
		tb.addAction(self.nextFile)
	
		# tb1 = My.videoEditor.addToolBar("Shortcut")
		# start1 = QAction("1", My.videoEditor)
		# start1.setShortcut("1")
		# start1.triggered.connect(self.getFiles)
		# tb1.addAction(start1)
	
		# Label Toolbar
		My.mainWindow.addToolBarBreak(Qt.TopToolBarArea)
		tb_label = My.mainWindow.addToolBar("Label Shortcuts")
		#create label tabs
		for key, value in self.labdict.items():
			labeltext = key + " = " + value
			label = QAction(labeltext, My.mainWindow)
			label.setShortcut(value)
			function = self.getFunction(key)
			label.triggered.connect(function)
			tb_label.addAction(label)
	
		# toolbar with video buttons
		My.mainWindow.addToolBarBreak(Qt.TopToolBarArea)
		tb_video = My.mainWindow.addToolBar("Video Shortcuts")
	
		prevframe = QAction("\u25c4 Previous Frame", My.mainWindow)
		prevframe.setShortcut(Qt.Key_Left)
		prevframe.triggered.connect(self.events.onButtonFramePrev)
		tb_video.addAction(prevframe)
	
		playpause = QAction("Play/Pause", My.mainWindow)
		playpause.setShortcut(Qt.Key_Space)
		playpause.triggered.connect(self.events.onButtonPlayPause)
		tb_video.addAction(playpause)
	
		nextframe = QAction("Next Frame \u25ba", My.mainWindow)
		nextframe.setShortcut(Qt.Key_Right)
		nextframe.triggered.connect(self.events.onButtonFrameNext)
		tb_video.addAction(nextframe)
	
		slower = QAction("\u25bc slower", My.mainWindow)
		slower.setShortcut(Qt.Key_Down)
		slower.triggered.connect(self.events.onButtonPlaySlower)    # final function is still missing
		tb_video.addAction(slower)
	
		faster = QAction("\u25b2 faster", My.mainWindow)
		faster.setShortcut(Qt.Key_Up)
		faster.triggered.connect(self.events.onButtonPlayFaster)    # final function is still missing
		tb_video.addAction(faster)
	
	# === file menu methods ===>
	
	def getFiles(self):
		filepaths = [os.path.abspath('data\\'+str(x)) for x in os.listdir("data")]
		aviFilepaths = list(filter(lambda x: x.endswith(".avi"),filepaths))
		jsonFilepaths = list(map((lambda x: x[:-5]), list(filter(lambda x: x.endswith(".csv"), filepaths))))
		self.selectedFiles = []
		for aviFile in aviFilepaths:
			if aviFile[:-10] not in jsonFilepaths:
				self.selectedFiles.append(aviFile)
		self.indexFile = -1
		self.setFilenumberText()
		self.nextFile.setDisabled(False)
		self.previousFile.setDisabled(False)
	
	def getNextFile(self):
		if self.isRange(self.indexFile+1):
			self.indexFile += 1
			self.loadNewFile()
	
	def getPreviousFile(self):
		if self.isRange(self.indexFile-1):
			self.indexFile -= 1
			self.loadNewFile()
	
	def loadNewFile(self):
		filepath = self.selectedFiles[self.indexFile]
		self.setFilenumberText()
		self.events.loadFiles(filepath)
	
	def setFilenumberText(self):
		self.filenumber.setText(str(self.indexFile+1) + " von " + str(len(self.selectedFiles)))
	
	def isRange(self, index):
		return index in range(0, len(self.selectedFiles))
	
	def loadLabelNames(self):
		file = open(My.Config.folder + '/' + My.Label.typeFile, 'r')
		items = json.load(file)
		for item in items:
			self.labdict[item['LabelName']] = item['Hotkey']
	
	# Function that returns another function for shortkeys
	def getFunction(self, text):
		# this function first sets the Label in the Combobox and than calls the button event to add the label
		def fn():
			index = self.cmbLabName.findText(text)
			print(index)
			self.cmbLabName.setCurrentIndex(index)
			self.events.onButtonAppend()
		return fn
